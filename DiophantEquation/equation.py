from random import randint, random, choice, uniform
import sympy.parsing.sympy_parser as parser
from sympy.utilities.lambdify import lambdify
from math import exp
from collections import namedtuple

RANDOM_PARAMETRES = [False, True][0]

EQUATION_NUM = 2
POPULATION_SIZE = 30
ITERATIONS = 50
CREATE_NEW_POPS = [False, True][1]
COMMON_MUTATION_CHANCE = 0.3  # for new pops True if 0.4-0.6 ~ OK for new pops False  0.55 ~ OK
MUTATION_CHANCE_FOR_WORST = 0.6
MUTATION_CHANCE_FOR_BEST = 0.15
SAVE_ANCESTOR_AFTER_MUTATATION = [False, True][0]
SELECTION_PRIOR_FUNC = 2
CROSSOVER_PRIOR_FUNC = 2
BREEDING_WAY = 1
MUTATION_WAY = 2
SEARCH_BOUNDARY = 30
# Modules of all Xi variables to be searched are not great then max of specified SERCH_BOUNDARY value and free term in equation
NESSESARY_BEST_PART = 0.15
NESSESARY_WORST_PART = 0.15

if RANDOM_PARAMETRES:
    EQUATION_NUM = randint(1, 3)
    POPULATION_SIZE = randint(10, 50)
    ITERATIONS = randint(50, 500)
    CREATE_NEW_POPS = choice([False, True])
    COMMON_MUTATION_CHANCE = random()
    MUTATION_CHANCE_FOR_WORST = random()
    MUTATION_CHANCE_FOR_BEST = random()
    SAVE_ANCESTOR_AFTER_MUTATATION = choice([False, True])
    SELECTION_RIOR_FUNC = randint(1, 2)
    CROSSOVER_PRIOR_FUNC = randint(1, 3)
    BREEDING_WAY = randint(1, 2)
    MUTATION_WAY = randint(1, 2)
    SEARCH_BOUNDARY = randint(10, 30)
    NESSESARY_BEST_PART = uniform(0, 0.25)
    NESSESARY_WORST_PART = uniform(0, 0.25)
"""
    FIELD PROBABILITIES IN ITERATIONS LOG SHOWS LIKELYHOOD OF CURRENT INSTANCE !!PARENTS!! TO BE PICKED OUT FOR BREEDING
    NOT THE CHANCE OF CURRENT INSTANCE!!
"""


class Instance:
    probability = None

    def __init__(self, genes, unfitness, parent1=None, parent2=None,
                 p1_prob=None, p2_prob=None, mutated=False):
        self.genes = genes
        self.unfitness = unfitness
        self.parent1 = parent1
        self.parent2 = parent2
        self.p1_prob = p1_prob  # probability of parent1
        self.p2_prob = p2_prob  # probability of parent2
        self.mutated = mutated

    def __str__(self, i: int, variables: list):
        s = f"instance {i + 1:2d}) {'(mutated) ' if self.mutated else ' ' * 10}"
        s += f"{str(dict(list(zip(variables, self.genes)))):23}; "
        s += f"unfitness: {self.unfitness:3d}; "
        if not (self.parent1 is None or self.parent2 is None):
            s += f"parents: {self.parent1 + 1:2d}, {self.parent2 + 1:2d}; "
            s += f"probabilities: {self.p1_prob:.3f}, {self.p2_prob:.3f}"
        s += '\n'
        return s


# class Population:
#     population_size = POPULATION_SIZE
#     instances = []
#     dynamic = []
#     average_unfitness = None

#     def __init__(self, instances):
#         self.instances = instances


class DiophantEquation:
    iterations = 0
    population = []
    parents_pairs = []
    solutions = dict()
    dynamic = []

    def __init__(self, equation, n=POPULATION_SIZE):
        eq = parser.parse_expr(equation)
        vars_list = list(eq.free_symbols)
        vars_list.sort(key=str)
        free_term = abs(eq.as_ordered_terms()[-1])
        self.equation = eq
        self.population_size = n
        self.best_part_boundary = int(round(n * NESSESARY_BEST_PART))
        self.worst_part_boundary = int(round(n * (1 - NESSESARY_WORST_PART)))
        self.vars_list = vars_list
        self.free_term = free_term
        self.search_boundary = min(free_term, SEARCH_BOUNDARY)
        self.vars_count = len(vars_list)
        self.F = lambdify(vars_list, eq)

    def __str__(self):
        s = f"AVERAGE POPULATION UNFITNESS: {self.get_avg_unfitness():.3f}\n"
        for i, instance in enumerate(self.population):
            s += instance.__str__(i, self.vars_list)
        return s

    def sort_by_fitness(self):
        self.population.sort(key=lambda x: x.unfitness)

    def get_avg_unfitness(self):
        avg_unfitness = 0.
        for instance in self.population:
            avg_unfitness += instance.unfitness
        avg_unfitness = round(avg_unfitness / self.population_size, 2)
        self.dynamic.append(avg_unfitness)
        return avg_unfitness

    def get_probabilities(self, prior_func=SELECTION_PRIOR_FUNC):
        common_value, priority_func = 0, None
        if prior_func == 1:
            priority_func = lambda x: 1. / x
        elif prior_func == 2:
            priority_func = lambda x: 1 / x**0.5
        for instance in self.population:
            common_value += priority_func(instance.unfitness)
        for instance in self.population:
            instance.probability = priority_func(instance.unfitness) / common_value

    def create_instance(self):
        genes = []
        for __ in range(self.vars_count):
            genes.append(randint(-self.search_boundary, self.search_boundary))
        return Instance(genes, abs(self.F(*genes)))

    def check_solution(self):
        self.sort_by_fitness()
        i, new_solutions_count = 0, 0
        print_dict = {'random': ('RANDOMLY', 'BY CROSSING'), 'repeat': ('FOR THE FIRST TIME', 'REPEATEDLY')}
        replaced_instances = []
        while self.population[i].unfitness == 0 and i < self.population_size:
            instance, new_instance = self.population[i], self.create_instance()
            repeat_flag = tuple(instance.genes) in self.solutions.keys()
            random_flag = not (instance.parent1 is None or instance.parent2 is None)
            if not repeat_flag:
                new_solutions_count += 1
                self.solutions[tuple(instance.genes)] = (print_dict['random'][random_flag], f"iter {self.iterations}")
            s = f"SOLUTION {instance.genes} HAS BEEN FOUND {print_dict['repeat'][repeat_flag]} "
            s += f"IN INSTANCE {i + 1}) {print_dict['random'][random_flag]}"
            s += f"(PARENTS: {instance.parent1 + 1}, {instance.parent2 + 1})" if random_flag else ""
            s += f" ON ITERATION {self.iterations}-{self.iterations + 1} "
            s += f"(SOLUTION INSTANCE WAS REPLACED BY RANDOM GENE {new_instance.genes})"
            replaced_instances.append(instance.__str__(i, self.vars_list) + \
                                      f" -> {dict(list(zip(self.vars_list, new_instance.genes)))}; " +
                                      f"unfitness: {abs(self.F(*new_instance.genes))}\n")
            self.population[i] = new_instance
            print(s)
            if new_instance.unfitness == 0:
                continue
            else:
                i += 1
        self.get_probabilities()
        print(self)
        if len(replaced_instances):
            replaced_instances = ["REPLACING LIST: \n"] + replaced_instances
            for s in replaced_instances:
                print(s)
        return i

    def create_population(self):
        self.population = []
        for _ in range(self.population_size):
            self.population.append(self.create_instance())
        print("\n------------------------------------------------------\n" + "NEW POPUALTION")
        return self.check_solution()

    def new_population(self):
        self.get_probabilities()
        new_pop, self.parents_pairs = [], []
        count = 0
        for i in list(range(self.best_part_boundary)) + \
                 list(range(self.worst_part_boundary, self.population_size)):
            p1, p2 = i, self.probabilistic_choice()
            while {p1, p2} in self.parents_pairs or p1 == p2:
                p2 = self.probabilistic_choice()
            new = self.breed(p1, p2)  # create a child
            count += len(new)
            new_pop += new
            self.parents_pairs.append({p1, p2})
        while count < self.population_size:
            p1, p2 = self.probabilistic_choice(), self.probabilistic_choice()
            while {p1, p2} in self.parents_pairs or p1 == p2:
                p1, p2 = self.probabilistic_choice(), self.probabilistic_choice()
            new = self.breed(p1, p2)  # create a child
            if count + len(new) <= self.population_size:
                count += len(new)
                new_pop += new
            else:
                count += 1
                new_pop.append(new[0])
            self.parents_pairs.append({p1, p2})
        self.population = new_pop
        self.iterations += 1
        return self.check_solution()

    def breed(self, p1, p2, prior_func=CROSSOVER_PRIOR_FUNC, breed_way=BREEDING_WAY, mut_way=MUTATION_WAY):
        pop, genes_count, priority_func = self.population, self.vars_count, None
        prob1, prob2 = pop[p1].probability, pop[p2].probability
        if prior_func == 1:
            priority_func = lambda x: 1. / x
        elif prior_func == 2:
            priority_func = lambda x: 1 / x**0.5
        elif prior_func == 3:
            priority_func = lambda x: 1 + 100 * exp(-x / 10)
        prior1, prior2 = priority_func(pop[p1].unfitness), priority_func(pop[p2].unfitness)
        p1_part, part_test = None, None
        if prior1 >= prior2:
            # part_test = min(genes_count - 1, round(genes_count*(prob1/(prob1 + prob2))))
            p1_part = min(genes_count - 1, round(genes_count * (prior1 / (prior1 + prior2))))
        else:
            # part_test = max(1, round(genes_count*(prob1/(prob1 + prob2))))
            p1_part = max(1, round(genes_count * prior1 / (prior1 + prior2)))
        # print(f"part_test = {part_test}, p1_part = {p1_part}, prior1 = {prior1:.4f}, prior2 = {prior2:.4f}")
        genes = []
        if breed_way == 1:
            genes = pop[p1].genes[:p1_part] + pop[p2].genes[p1_part:]
        elif breed_way == 2:
            indeces = []
            for _ in range(p1_part):
                index = randint(0, genes_count - 1)
                while index in indeces:
                    index = randint(0, genes_count - 1)
                indeces.append(index)
            indeces.sort()
            # print(indeces)
            for i in range(genes_count):
                if i in indeces:
                    genes.append(pop[p1].genes[i])
                else:
                    genes.append(pop[p2].genes[i])
        unfitness = abs(self.F(*genes))
        child = Instance(genes, unfitness, p1, p2, prob1, prob2)
        if p1 < self.best_part_boundary and p2 < self.best_part_boundary:
            mut_chance = MUTATION_CHANCE_FOR_BEST
        elif p1 >= self.worst_part_boundary and p2 >= self.worst_part_boundary:
            mut_chance = MUTATION_CHANCE_FOR_WORST
        else:
            mut_chance = COMMON_MUTATION_CHANCE
        result, mut_genes = [], genes[:]
        for i in range(genes_count):
            if random() <= mut_chance:
                if mut_way == 1:
                    mut_genes[i] = randint(-self.search_boundary, self.search_boundary)
                elif mut_way == 2:
                    sign = '-' if mut_genes[i] < 0 else ''
                    binary = bin(abs(mut_genes[i]))[2:]
                    pos = randint(0, len(binary) - 1) if len(binary) > 1 else 0
                    tmp = binary[:pos] + str((int(binary[pos]) + 1) % 2)
                    binary = tmp + binary[pos + 1:] if pos < len(binary) - 1 else tmp
                    if int(binary, base=2) > self.search_boundary:
                        binary = '0' + binary[1:]
                    mut_genes[i] = int(sign + binary, base=2)
        if mut_genes != genes:
            unfitness = abs(self.F(*mut_genes))
            mutant = Instance(mut_genes, unfitness, p1, p2, prob1, prob2, True)
            result.append(mutant)
            if SAVE_ANCESTOR_AFTER_MUTATATION:
                result.append(child)
        else:
            result.append(child)
        return result

    def probabilistic_choice(self):
        i, raffle = -1, random()
        while raffle > 0:
            i += 1
            raffle -= self.population[i].probability
        return i

    def find_roots(self, n=1, maxiters=50, create_new_pops=CREATE_NEW_POPS):
        solutions = self.solutions
        if create_new_pops:
            while len(solutions) < n: #and self.iterations < maxiters:
                iters, self.dynamic, self.parents_pairs = 0, [], []
                if self.create_population() > 0:
                    continue
                while True:
                    iters += 1
                    print("--------------------------------------------------")
                    print(f"ITERATION {iters}({self.iterations + 1})")
                    if self.new_population() > 0 or self.iterations == maxiters:
                        break
                print("--------------------------------------------------\n")
                dynamic = ""
                for i in range(iters):
                    dynamic += f"{i}) {self.dynamic[i]} -> "
                dynamic += f"{iters}) {self.dynamic[iters]}"
                print(f"POPULATION AVERAGE UNFITNESS DYNAMIC: {dynamic}")
        else:
            self.create_population()
            while len(solutions) < n: #and self.iterations < maxiters:
                print("--------------------------------------------------")
                print(f"ITERATION ({self.iterations + 1})")
                self.new_population()
                print("--------------------------------------------------\n")
            dynamic = ""
            for i in range(self.iterations):
                dynamic += f"{i}) {self.dynamic[i]} -> "
            dynamic += f"{self.iterations}) {self.dynamic[self.iterations]}"
            print(f"POPULATION AVERAGE UNFITNESS DYNAMIC: {dynamic}")
        print(f"AFTER {self.iterations} ITERATIONS WITH {self.population_size} INSTNCES " +
              f"FOUNDED {len(solutions)} SOLUTIONS FOR {self.equation} = 0 EQUATION:")
        count = 0
        for solution in solutions.keys():
            count += 1
            print(f'{count}) {solution} {solutions[solution]}')
            """dict(list(zip(self.vars_list, solution)))"""


equations = dict()
Equation = namedtuple('Equation', ['expr', 'roots_count'])
equations[1] = Equation("x**2+y**2-25", 12)
equations[2] = Equation("x**2+y**2+z**2-17", 1) #n = 48
equations[3] = Equation("a+2*b+3*c+4*d-30", 100)
equation = DiophantEquation(equations[EQUATION_NUM].expr)
equation.find_roots(n=equations[EQUATION_NUM].roots_count, maxiters=ITERATIONS)
