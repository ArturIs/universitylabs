const int SEARCH_AREA_BORDER =  15;

const int POPULATION_SIZE = 20;
const double NECESSARY_BEST_PART = 0.15;
const double NECESSARY_WORST_PART = 0.15;
const int POPULATION_PRIORITY_FUNC_ID = 1;
const int CROSSOVER_PRIORITY_FUNC_ID = 1;
const double COMMON_MUTATION_CHANCE = 0.5;
const double MUTATION_CHANCE_FOR_WORST = 0.7;
const double MUTATION_CHANCE_FOR_BEST = 0.15;

const int ITERATIONS_MAX_NUMBER = 1;


struct InstanceParameters {
    int genes_count = -1;
    int max_value = SEARCH_AREA_BORDER;
};

struct PopulationParameters {
    const int size = POPULATION_SIZE;
};



double (* POPULATION_PRIORITY_FUNC)(double) =
POPULATION_PRIORITY_FUNC_ID == 1 ? [](double x) -> double { return pow(1/x, 0.5); } : [](double x) -> double { return 1/x; };


double (* CROSSOVER_PRIORITY_FUNC)(double) =
CROSSOVER_PRIORITY_FUNC_ID == 1 ? [](double x) -> double { return pow(1/x, 0.5); } : [](double x) -> double { return 1/x; };






