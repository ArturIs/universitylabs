#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <random>
#include "params.h"

//-------------------
#include <fmt/core.h>
#include "exprtk.hpp"  // СТОРОННЯЯ БИБЛИОТЕКА ДЛЯ ПРЕОБРАЗОВАНИЯ СТРОК С МАТЕМАТИЧЕСКИМИ ВЫРАЖЕНИЯ В ФУНКЦИИ
//-------------------

using std::vector, std::map, std::set, std::pair;
using std::string, std::cout, std::endl;
using fmt::format;


std::random_device rd;
std::mt19937 mt(rd());
std::uniform_int_distribution<> dist_genes(-SEARCH_AREA_BORDER, SEARCH_AREA_BORDER);
std::uniform_real_distribution<> dist_chance(0., 1.);


InstanceParameters INSTANCE_PARAMETERS;
PopulationParameters POPULATION_PARAMETERS;
map<vector<int>, string> SOLUTIONS;

class Equation;
class Instance;
class Population;
class Solver;


class Instance {
const InstanceParameters &params = INSTANCE_PARAMETERS;
private:
    static int genes_count;

    vector<int> genes;
    int parent1 = -1, parent2 = -1;
    double parent1_probability = 0, parent2_probability = 0;
    double unfitness = 0;
    double reproduction_probability = 0;
    string mutations = "";

    void set_random_genes();
    void compute_unfitness();

    friend class Equation;
    friend class Population;
    friend class Solver;
public:
    Instance();
    Instance(const vector<int> &given_genes, int p1, int p2, double p1_prob, double p2_prob);
    Instance(const Instance &instance);

    bool operator<(const Instance & other_instance) const { return this->unfitness < other_instance.unfitness; }
    void operator=(const Instance &other_instance);

    string genes_str() const;
    string str_representation() const;
};
int Instance::genes_count = 0;


class Population {
const PopulationParameters &params = POPULATION_PARAMETERS;
private:
    static const int size = POPULATION_SIZE;
    static double average_unfitness;
    static double (* population_priority_func)(double unfitness);
    static double (* crossover_priority_func)(double unfitness);
    
    vector<Instance> instances;
    double best_part_boundary = int(round(params.size * NECESSARY_BEST_PART));
    double worst_part_boundary = int(round(params.size * (1 - NECESSARY_WORST_PART)));
    friend class Solver;

    map<string, int>  check_solutions();
    map<string, int>  new_generation();
    void get_probabilities();
    int probabilistic_choice();
    Instance breed(int p1, int p2);
public:
    string str_representation();
};
double Population::average_unfitness = 0;
double (*Population::population_priority_func)(double) = POPULATION_PRIORITY_FUNC;
double (*Population::crossover_priority_func)(double) = CROSSOVER_PRIORITY_FUNC;


class Solver {
private:
    Population population;

    friend Population;
public:
    static int iterations;
    void find_roots(int roots_count, int maxiters);
    map<string, int> new_population();
};
int Solver::iterations = 0;


class Equation {
private:
    exprtk::expression<double> equation;
    int vars_count = 0;
    vector<double> vars_values;
    vector<string> vars_list;

    friend Instance;
    friend Population;
    friend Solver;
public:
    string  equation_string;
    Equation() = default;
    Equation(const string &equation_string);
};
Equation EQUATION;



//----------------------------------------------------------------------------------------------------------------------
//EQUATION CLASS       EQUATION CLASS       EQUATION CLASS       EQUATION CLASS       EQUATION CLASS
Equation::Equation(const string &equation_string) {
    this->equation_string = equation_string;
    exprtk::collect_variables(equation_string, vars_list);
    vars_count = vars_list.size();
    vars_values.resize(vars_count);
    INSTANCE_PARAMETERS.genes_count = vars_count;
    Instance::genes_count = vars_count;

    exprtk::symbol_table<double> symbol_table;
    for (int i = 0; i < vars_count; ++i) {
        symbol_table.add_variable(vars_list[i], vars_values[i]);
    }
    symbol_table.add_constants();
    equation.register_symbol_table(symbol_table);
    exprtk::parser<double> parser;
    parser.compile(equation_string, equation);
}

//EQUATION CLASS       EQUATION CLASS       EQUATION CLASS       EQUATION CLASS       EQUATION CLASS
//----------------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------------
//INSTANCE CLASS    INSTANCE CLASS      INSTANCE CLASS   INSTANCE CLASS      INSTANCE CLASS   INSTANCE CLASS
//КОНСТРУКТОР ПО УМОЛЧАНИЮ
Instance::Instance() {
    set_random_genes();
    compute_unfitness();
}


//КОНСТРУКТОР ДЛЯ ИНИЦИАЛИЗАЦИИ ОСОБЕЙ ПОТОМКОВ
Instance::Instance(const vector<int> &given_genes, int p1 = -1, int p2 = -1, double p1_prob = 0, double p2_prob = 0):
    parent1 {p1}, parent2 {p2}, parent1_probability {p1_prob}, parent2_probability {p2_prob} {
    genes.resize(genes_count);
    for (int i = 0; i < genes_count; ++i) {
        genes[i] = given_genes[i];
    }
    compute_unfitness();
}


Instance::Instance(const Instance &instance) {
    *this = instance;
}


//ИНИЦИАЛИЗУЕТ ОСОБЬ СЛУЧАЙНЫМ НАБОРОМ ГЕНОВ
void Instance::set_random_genes() {
    genes.resize(genes_count);
    for (int &gene: genes) {
        gene = dist_genes(mt);
    }
}

//ВЫЧИСЛЯЕТ НЕВЯЗКУ ЗАДАННОГО НАБОРА ЗНАЧЕНИЙ ПЕРЕМЕННЫХ ПРИ ПОДСТАНОВКЕ В УРАВНЕНИЕ
void Instance::compute_unfitness() {
    for(int i = 0; i < genes_count; ++i) {
        EQUATION.vars_values[i] = genes[i];
    }
    unfitness = abs(EQUATION.equation.value());
}


//СТРОКА СО ЗНАЕЧНИЯМИ ГЕНОВ
string Instance::genes_str() const {
    string str = "{";
    for (int i = 0; i < genes_count - 1; ++i) {
        str += format("{} = {:>+3d}, ", EQUATION.vars_list[i], genes[i]);
    }
    str += format("{} = {:>+3d}}}", EQUATION.vars_list[genes_count - 1], genes[genes_count - 1]);
    return str;
}


//СТРОКА ДЛЯ ПЕЧАТИ ДАННЫХ ОБ ОСОБИ
string Instance::str_representation() const {
    string str = format("genes: {};   ", genes_str());
    str += format("unfitness: {:>6.2f};", unfitness);
    //str += format("reproduction probability: {:>.3f}   ", reproduction_probability);
    if (not (parent1 < 0 or parent2 < 0)) {
        str += format("   parents: {:>2d}, {:>2d};   ", parent1 + 1, parent2 + 1);
        str += format("parents probabilities: {:>.3f}, {:>.3f}", parent1_probability, parent2_probability);
    }
    if (not mutations.empty()) {
        str += format("   (mutated: {})", mutations);
    }
    return str;
}


void Instance::operator=(const Instance &instance) {
    this->genes = instance.genes;
    this->unfitness = instance.unfitness;
    this->parent1 = instance.parent1;
    this->parent2 = instance.parent2;
    this->reproduction_probability = instance.reproduction_probability;
    this->parent1_probability = instance.parent1_probability;
    this->parent2_probability = instance.parent2_probability;
    this->mutations = instance.mutations;
}
//INSTANCE CLASS    INSTANCE CLASS      INSTANCE CLASS   INSTANCE CLASS      INSTANCE CLASS   INSTANCE CLASS
//----------------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------------
//POPULATION CLASS      POPULATION CLASS        POPULATION CLASS      POPULATION CLASS        POPULATION CLASS
//СТРОКА ДЛЯ ВЫВОДА ИНФОРМАЦИИ О ТЕКУЩЕЙ ПОПУЛЯЦИИ
string Population::str_representation() {
    string str = format("AVERAGE UNFITNESS: {:>6.2f}\n", average_unfitness);
    for (int i = 0; i < size; ++i) {
        str += format("Instance {:>2d})   {}\n", i+1, instances[i].str_representation());
    }
    return str;
}


//ПРОВЕРЯЕТ, ЕСТЬ ЛИ В ТЕКУЩЕЙ ПОПУЛЯЦИИ ОСОБИ, ЯВЛЯЮЩИЕСЯ РЕШЕНИЕМ.
//ЕСЛИ ДА, ДОБАВЛЯЕТ ИХ В МНОЖЕСТВО РЕШЕНИЙ И ЗАМЕНЯЕТ ДРУГИМИ СЛУЧАЙНЫМИ ОСОБЯМИ
map<string, int> Population::check_solutions() {
    std::sort(instances.begin(), instances.end());
    int i = 0, all_solutions_count = 0, new_solutions_count = 0;
    map<string, vector<string>> print_dict;
    print_dict["random"] = {"BY CROSSING", "RANDOMLY"};
    print_dict["repeat"] = {"FOR THE FIRST TIME", "REPEATEDLY"};
    vector<string> replaced_instances;

    while (instances[i].unfitness == 0 and i < size) {
        all_solutions_count++;
        Instance new_instance;
        int repeat_flag = SOLUTIONS.count(instances[i].genes);
        int random_flag = instances[i].parent1 < 0 or instances[i].parent2 < 0;
        if (not repeat_flag) {
            new_solutions_count += 1;
            SOLUTIONS[instances[i].genes] = print_dict["random"][random_flag];
        }

        string s = format("SOLUTION {} ", instances[i].genes_str());
        s += format("HAS BEEN FOUND {} ", print_dict["repeat"][repeat_flag]);
        s += format("IN INSTANCE {:d} {} ", i+1, print_dict["random"][random_flag]);
        s += random_flag ? format("(PARENTS: {:d} {:d}) ", instances[i].parent1 + 1, instances[i].parent2 + 1) : "";
        s += format("ON ITERATION {:d}-{:d} ", Solver::iterations, Solver::iterations + 1);
        s += format("(SOLUTION INSTANCE WAS REPLACED BY RANDOM INSTANCE {})\n", new_instance.genes_str());
        //cout << s;
        replaced_instances.push_back(format("Instance {:2d})   {}  --->  {};   unfitness: {}\n",
            i + 1, instances[i].str_representation(), new_instance.genes_str(), new_instance.unfitness));

        instances[i] = new_instance;
        if (new_instance.unfitness == 0) {
            continue;
        } else {
            i++;
        }
    }

    get_probabilities();
    average_unfitness = 0;
    for (const auto &instance : instances){
        average_unfitness += instance.unfitness;
    }
    average_unfitness /= size;
    //cout << '\n' << this->str_representation();
    if (not replaced_instances.empty()) {
        //cout << "\nREPLACING LIST:\n";
        for (const string &s: replaced_instances) {
            //cout << s;
        }
    }
    
    map<string, int> solutions_founded({{"all", all_solutions_count}, {"new", new_solutions_count}});
    return solutions_founded;
}


//ОПРЕДЕЛЯЕТ ДЛЯ КАЖДОЙ ОСОБОИ ВЕРОЯТНОСТЬ С КОТОРОЙ ОНА ОСТАВИТ ПОТОМСТВО
void Population::get_probabilities()  {
    double common_value = 0;
    for (const auto &instance : instances) {
        common_value += population_priority_func(instance.unfitness);
    }
    for (auto &instance : instances) {
        instance.reproduction_probability = population_priority_func(instance.unfitness) / common_value;
    }
}


//СКРЕЩИВАЕТ ОСОБЕЙ P1 И P2; ОСТАВЛЯЕТ ПОТОМКА В ПОПУЛЯЦИИ, ПРЕДКОВ НЕТ
Instance Population::breed(int p1, int p2) {
    double prior1, prior2;
    int p1_part, &genes_count = EQUATION.vars_count;
    vector<int> genes;
    prior1 = crossover_priority_func(instances[p1].unfitness);
    prior2 = crossover_priority_func(instances[p2].unfitness);
    if (prior1 >= prior2) {
        p1_part = fmin(genes_count - 1, int(round(genes_count * (prior1 / (prior1 + prior2)))));
    } else {
        p1_part = fmax(1, int(round(genes_count * prior1 / (prior1 + prior2))));
    }
    for (int i = 0; i < p1_part; ++i) {
        genes.push_back(instances[p1].genes[i]);
    }
    for (int i = p1_part; i < genes_count; ++i) {
        genes.push_back(instances[p2].genes[i]);
    }

    Instance child(genes, p1, p2, instances[p1].reproduction_probability, instances[p2].reproduction_probability);
    double mutation_chance;
    if (p1 <= best_part_boundary and p2 <= best_part_boundary) {
        mutation_chance = MUTATION_CHANCE_FOR_BEST;
    } else if (p1 >= worst_part_boundary and p2 >= worst_part_boundary) {
        mutation_chance = MUTATION_CHANCE_FOR_WORST;
    } else {
        mutation_chance = COMMON_MUTATION_CHANCE;
    }

    for (int i = 0; i < genes_count; ++i) {
        if (dist_chance(mt) <= mutation_chance) {
            int tmp = child.genes[i];
            do {
                child.genes[i] = dist_genes(mt);
            } while (tmp == child.genes[i]);
            child.mutations += format("{0}({1:>+3d})->{0}({2:>+3d}), ", EQUATION.vars_list[i], tmp, child.genes[i]);
        }
    }
    if (not child.mutations.empty()) {
        child.mutations.pop_back();
        child.mutations.pop_back();
        child.compute_unfitness();
    }

    return child;
}


//ОБНОВЛЯЕТ ПОПУЛЯЦИЮ, СКРЕЩИВАЯ ОСОБИ
map<string, int>  Population::new_generation() {
    vector<Instance> new_generation;
    vector<int> necessary_instances;
    set<set<int>> parents_pairs;
    for (int i = 0; i < size; ++i) {
        if (i <= best_part_boundary or i >= worst_part_boundary) {
            necessary_instances.push_back(i);
        }
    }

    int new_count = 0;
    for (const int &p1: necessary_instances) {
        int p2;
        do {
            p2 = probabilistic_choice();
        } while (parents_pairs.count({p1, p2}) > 0 or p1 == p2);
        new_generation.push_back(breed(p1, p2));
        parents_pairs.insert({p1, p2});
        new_count++;
    }

    while (new_count < size) {
        int p1, p2;
        do {
            p1 = probabilistic_choice();
            p2 = probabilistic_choice();
        } while (parents_pairs.count({p1, p2}) > 0 or p1 == p2);
        new_generation.push_back(breed(p1, p2));
        parents_pairs.insert({p1, p2});
        new_count++;
    }

    instances = new_generation;
    return check_solutions();
}


int Population::probabilistic_choice() {
    int p = -1;
    double raffle = dist_chance(mt);
    while (raffle > 0) {  // "raffle" - розыгрыш, лотерея
        p++;
        raffle -= instances[p].reproduction_probability;
    }
    return p;
}
//POPULATION CLASS      POPULATION CLASS        POPULATION CLASS      POPULATION CLASS        POPULATION CLASS
//----------------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------------
//SOLVER CLASS      SOLVER CLASS        SOLVER CLASS      SOLVER CLASS        SOLVER CLASS      SOLVER CLASS
//КОНСТРУКТОР

map<string, int> Solver::new_population() {
    population.instances.resize(Population::size);
    return population.check_solutions();
}

void Solver::find_roots(int roots_count, int maxiters=ITERATIONS_MAX_NUMBER) {
    int populations_count = 0;
    while (SOLUTIONS.size() < roots_count) {// and iterations < maxiters) {
        vector<string> unfitness_dynamic;
        int iters = 0;
        populations_count++;
        //cout << "\n-----------------------------------------------------------------------------\n";
        //cout << format("NEW POPULATION (#{:d})", populations_count);
        if (new_population()["new"] > 0) {
            continue;
        }
        while (true) {
            iterations++;
            iters++;
            //cout << format("ITERATION {:d}({:d})\n", iters, iterations);
            if (population.new_generation()["all"] > 0 or iterations == maxiters) {
                break;
            }
            //cout << "\n-----------------------------------------------------------------------------\n";
        }
    }

//    cout << format("\nAFTER {:d} ITERATIONS WITH {:d} INSTANCES - {:d} SOLUTIONS FOR EQUATION \"{} = 0\" FOUNDED:\n",
//                   iterations, POPULATION_SIZE, SOLUTIONS.size(), EQUATION.equation_string);
//    int i = 0;
//    for (const auto &el : SOLUTIONS) {
//        ++i;
//        cout << format("{:d})  {}  {}\n", i, Instance(el.first).genes_str(), el.second);
//    }
    return;
}
//SOLVER CLASS      SOLVER CLASS        SOLVER CLASS      SOLVER CLASS        SOLVER CLASS      SOLVER CLASS
//----------------------------------------------------------------------------------------------------------------------