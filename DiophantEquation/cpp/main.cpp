#include "classes.h"
#include <omp.h>
#include <ctime>

int main() {
    setlocale(LC_ALL, "RUSSIAN");
    std::string equation_string = "x^2 + y^2 + z^2 - 17";
    EQUATION = Equation(equation_string);


    int start_time = clock();
    #pragma omp parallel num_threads(1)
    {
        //cout << "Hello";
        //cout << " OK";
        Solver solver;
        solver.find_roots(20);

        #pragma omp single
        {

            cout << format(
                    "\nAFTER {:d} ITERATIONS WITH {:d} INSTANCES - {:d} SOLUTIONS FOR EQUATION \"{} = 0\" FOUNDED:\n",
                    Solver::iterations, POPULATION_SIZE, SOLUTIONS.size(), EQUATION.equation_string);
            int i = 0;
            for (const auto &el : SOLUTIONS) {
                ++i;
                cout << format("{:d})  {}  {}\n", i, Instance(el.first).genes_str(), el.second);
            }
        }
    }
    int end_time = clock();
    cout <<  "NUM THREADS 1: TIME = " << end_time - start_time << endl;


    start_time = clock();
    #pragma omp parallel num_threads(4)
    {
        //cout << "Hello";
        //cout << " OK";
        Solver solver;
        solver.find_roots(20);
        #pragma omp single
        {

            cout << format(
                    "\nAFTER {:d} ITERATIONS WITH {:d} INSTANCES - {:d} SOLUTIONS FOR EQUATION \"{} = 0\" FOUNDED:\n",
                    Solver::iterations, POPULATION_SIZE, SOLUTIONS.size(), EQUATION.equation_string);
            int i = 0;
            for (const auto &el : SOLUTIONS) {
                ++i;
                cout << format("{:d})  {}  {}\n", i, Instance(el.first).genes_str(), el.second);
            }
        }
    }
    end_time = clock();
    cout <<  "NUM THREADS 4: TIME = " << end_time - start_time << endl;



    start_time = clock();
    #pragma omp parallel num_threads(8)
    {
        //cout << "Hello";
        //cout << " OK";
        Solver solver;
        solver.find_roots(20);

        #pragma omp single
        {

            cout << format(
                    "\nAFTER {:d} ITERATIONS WITH {:d} INSTANCES - {:d} SOLUTIONS FOR EQUATION \"{} = 0\" FOUNDED:\n",
                    Solver::iterations, POPULATION_SIZE, SOLUTIONS.size(), EQUATION.equation_string);
            int i = 0;
            for (const auto &el : SOLUTIONS) {
                ++i;
                cout << format("{:d})  {}  {}\n", i, Instance(el.first).genes_str(), el.second);
            }
        }
    }
    end_time = clock();
    cout <<  "NUM THREADS 8: TIME = " << end_time - start_time << endl;

}
